(function () {
    "use strict";

    const PARTICIPANTS_LIST = [];
    let theWheel;
    let wheelPower = 0;
    let wheelSpinning = false;

    function refreshWheel() {
        let participants = PARTICIPANTS_LIST.slice(0);
        if (participants.length < 1) {
            participants = [
                'INSERT',
                'NAMES',
                'INSERT',
                'NAMES',
                'INSERT',
                'NAMES',
                'INSERT',
                'NAMES',
                'INSERT',
                'NAMES'
            ];
        }
        let segments = participants.map(function(n){
            return {
                fillStyle: getRandomColor(),
                text: n.toTitleCase()
            }
        });
        theWheel = new Winwheel({
            'numSegments'   : segments.length,   // Specify number of segments.
            'outerRadius'   : 212,  // Set radius to so wheel fits the background.
            'innerRadius'   : 120,  // Set inner radius to make wheel hollow.
            'textFontSize'  : 16,   // Set font size accordingly.
            'textMargin'    : 0,    // Take out default margin.
            'segments'      : segments,      // Define segments including colour and text.
            'animation' :           // Define spin to stop animation.
                {
                    'type'     : 'spinToStop',
                    'duration' : 5,
                    'spins'    : 8,
                    'callbackFinished' : alertPrize
                }
        });
    }

    function startSpin()
    {
        // Ensure that spinning can't be clicked again while already running.
        if (wheelSpinning == false)
        {

            theWheel.rotationAngle = 0;     // Re-set the wheel angle to 0 degrees.

            theWheel.animation.spins = 3 + Math.floor(Math.random()*12);

            // Begin the spin animation by calling startAnimation on the wheel object.
            theWheel.startAnimation();

            // Set to true so that power can't be changed and spin button re-enabled during
            // the current animation. The user will have to reset before spinning again.
            wheelSpinning = true;
        }
    }

    function resetWheel()
    {
        theWheel.stopAnimation(false);  // Stop the animation, false as param so does not call callback function.
        theWheel.draw();                // Call draw to render changes to the wheel.
        wheelSpinning = false;          // Reset to false to power buttons and spin can be clicked again.
    }

    // -------------------------------------------------------
    // Called when the spin animation has finished by the callback feature of the wheel because I specified callback in the parameters.
    // note the indicated segment is passed in as a parmeter as 99% of the time you will want to know this to inform the user of their prize.
    // -------------------------------------------------------
    function alertPrize(indicatedSegment)
    {
        // Do basic alert of the segment text. You would probably want to do something more interesting with this information.
        alert("La licenza é stata vinta da\n" + indicatedSegment.text.replace("\n", " "));
        resetWheel();
    }

    function populateList() {
        let part_ul = document.getElementById('participants-list');
        part_ul.innerHTML = '';

        for (let i = 0; i < PARTICIPANTS_LIST.length; i++) {
            let part_li = document.createElement("LI");
            let part_label = document.createElement("SPAN");
            part_label.innerText = PARTICIPANTS_LIST[i].toTitleCase();
            part_li.appendChild(part_label);
            part_ul.appendChild(part_li);
        }
    }

    function promptName() {
        let name = window.prompt('Inserire il nome').toLowerCase();
        if (!!name && PARTICIPANTS_LIST.indexOf(name) < 0) {
            PARTICIPANTS_LIST.push(name);
            PARTICIPANTS_LIST.sort();
        }
        refreshWheel();
        populateList();
    }

    function getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return "#"+((1<<24)*Math.random()|0).toString(16);
    }


    function run() {
        let add_btn = document.getElementById("add_name_btn");
        let spin_btn = document.getElementById("spin_btn");
        add_btn.addEventListener("click", promptName);
        spin_btn.addEventListener("click", startSpin);
        refreshWheel();



    }


    document.addEventListener('DOMContentLoaded', run);

    String.prototype.toTitleCase = function() {
        return this.replace(/\w\S*/g, function(txt){
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    }

}());